# Routing Table - Publish Subscriber Pattern


- Como exemplo de implementação de um Publisher Subscriber Pattern estarei utilizando como exemplo de fonte de dados
  uma tabela de roteamento que no nosso caso será a publisher (fonte dos dados).

- Cada entrada dessa tabela terá uma ponteiro para sua propria notification chain. Como cada entrada pode ter N subscribers, será utilizada uma linked list para gerenciar esses subscribers.







