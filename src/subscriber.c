#include<stdio.h>
#include<memory.h>
#include<pthread.h>
#include<stdlib.h>
#include<sys/types.h>
#include<stdbool.h>
#include<stdint.h>

#include"routing_table.h"

extern routing_table_t* get_publisher_rt(void);

static void *subscriber(void* param){

    /* subscriber precisa dizer em qual entrada da routing table ele quer se inscrever
       e ser notificado sobre quaisquer mudanças. */

    routing_table_keys_t rt_keys;
    memset(&rt_keys, 0, sizeof(routing_table_keys_t));
    strncpy(rt_keys.destination, "192.168.0.1", DEST_KEY_SIZE);
    rt_keys.mask = 32;
}

void init_subscriber_thread(uint32_t sub_id){

    pthread_t subscriber_thread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&subscriber_thread, &attr, subscriber, (void *)sub_id);
    printf("[+] Subscriber: [ %d ]\n", sub_id);
}

