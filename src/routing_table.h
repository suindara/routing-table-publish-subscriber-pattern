#ifndef ROUTING_TABLE_H
#define ROUTING_TABLE_H

#include <stdbool.h>
#include"notification_chain.h"

typedef struct routing_table_keys_t routing_table_keys_t; /* representa as chaves */
typedef struct routing_table_entry_t routing_table_entry_t;
typedef struct routing_table_t routing_table_t;

typedef enum{
    UPDATE,
    INSERT,
    DELETE
}entry_type;

#define DEST_KEY_SIZE 16
#define GATEWAY_IP_SIZE 16
#define OUTGOING_INTERFACE_SIZE 32

struct routing_table_keys_t{
    char destination[DEST_KEY_SIZE];
    char mask;
};

struct routing_table_entry_t{
    routing_table_keys_t rt_keys;
    char gateway_ip[GATEWAY_IP_SIZE];
    char outgoing_interface[OUTGOING_INTERFACE_SIZE];
    bool created;
    notification_chain_t* notification_chain;
    routing_table_entry_t* prev;
    routing_table_entry_t* next;
};

struct routing_table_t{
    routing_table_entry_t* head;
    unsigned int size;
};


void init_routing_table(routing_table_t* r);
void dump_routing_table_entry(routing_table_t* rt);
void routing_table_register_notification(routing_table_t*, routing_table_keys_t*, size_t /*key size*/, callback_fn, unsigned int /*subscriber identifier*/);
bool delete_routing_table_entry(routing_table_t* r, const char* destination_addr, char mask);
routing_table_entry_t* peek_routing_table_entry(routing_table_t* rt, const char *destination_addr, char mask);
routing_table_entry_t* add_update_routing_table(routing_table_t *rt, const char* destination_addr, char mask, const char* gateway, const char* interface, entry_type type);

#endif