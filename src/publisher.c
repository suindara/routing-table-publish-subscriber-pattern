#include<pthread.h>
#include<unistd.h>
#include<stdio.h>
#include"routing_table.h"

routing_table_t publisher_routing_table;

routing_table_t* get_publisher_rt(void){
    return &publisher_routing_table;
}

extern void init_subscriber_thread(uint32_t sub_id);

void menu(void){

    int option = 0;

    while(1){

        printf("Menu\n");
        printf("[1] Inserir/Atualizar uma entrada na tabela.\n");
        printf("[2] Deletar uma entrada.\n");
        printf("[3] Imprimir a tabela.\n");
        scanf("%d", &option);

        switch (option)
        {
        case 1:
        {
            char destination_addr[DEST_KEY_SIZE];
            char mask;
            char interface[OUTGOING_INTERFACE_SIZE];
            char gateway[GATEWAY_IP_SIZE];
            printf("[+]Destino: \n");
            scanf("%s", destination_addr);
            printf("[+] Máscara: \n");
            scanf("%d", &mask);
            printf("[+] Interface: \n");
            scanf("%s", interface);
            printf("[+] Gateway: \n");
            scanf("%s", gateway);
            add_update_routing_table(get_publisher_rt(), destination_addr, mask, gateway, interface, INSERT);
            break;
        }
        case 2:
        {   
            char destination_addr[DEST_KEY_SIZE];
            char mask;
            printf("[-] Endereço: \n");
            scanf("%s", destination_addr);
            printf("[-] Máscara: \n");
            scanf("%d", &mask);
            delete_routing_table_entry(get_publisher_rt(), destination_addr, mask);
            break;
        }
        case 3:
            dump_routing_table_entry(get_publisher_rt());
            break;

        default:
            printf("Opção inválida!\n");
            menu();
            break;
        }
    }
}

/* publisher thread, nossa fonte de dados */
static void* publisher(void *param){

    routing_table_t* rt = get_publisher_rt();

    /* populando routing table */
    add_update_routing_table(rt, "192.168.0.1", 32, "10.1.1.1", "eth0", INSERT);
    add_update_routing_table(rt, "192.168.0.2", 32, "10.1.1.2", "eth0", INSERT);
    add_update_routing_table(rt, "192.168.0.3", 32, "10.1.1.3", "eth0", INSERT);
    add_update_routing_table(rt, "192.168.0.4", 32, "10.1.1.4", "eth0", INSERT);

    dump_routing_table_entry(rt);
    menu();
}

void init_publisher_thread(){

    pthread_t pub_pthread;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&pub_pthread, &attr, publisher, (void *)0);

}

int main(int argc, char** argv){

    init_routing_table(&publisher_routing_table);
    init_subscriber_thread(1);
    sleep(1);

    init_subscriber_thread(2);
    sleep(1);

    init_subscriber_thread(3);
    sleep(1);

    init_publisher_thread();
    printf("Publisher thread criada!\n");

    pthread_exit(0);

    return 0;
}