#include<stdlib.h>
#include<assert.h>
#include<memory.h>
#include<string.h>

#include"notification_chain.h"

/*cada entrada na route table tera uma notification chain*/
notification_chain_t* create_notification_chain(char* name){
    notification_chain_t* nc = calloc(1, sizeof(notification_chain_t));
    nc->head = NULL;
    nc->tail = NULL;
    
    if(name){
        strncpy(nc->name, name, MAX_NOTIFICATION_CHAIN_NAME);
    }
    
    return nc;
}

void add_element_into_notification_chain(notification_chain_t* nc, chain_element_t* ne){

    if(nc->head == NULL && nc->tail == NULL){
        nc->head = ne;
        nc->tail = ne;
        return;
    }

    chain_element_t* node = nc->tail;
    node->next = ne;
    nc->tail = ne;    
    ne->next = NULL;
}

void notification_chain_call(notification_chain_t * nc, void* param, unsigned int param_size, const char* key, unsigned int key_size, ntf_chain_option_t option){

    assert(key_size <= MAX_NOTIFICATION_KEY);

    /* pega uma chain_element e compara com a chave armazenada no element atual com a que foi passada por parametro,
        se a chave por parametro não foi especificada ou se a key_set do element não foi setada, quer dizer que é um wildcard
        e o subscriber quer receber notificação sobre quaisquer alterações de todos os elementos da route table */
    
    for(chain_element_t* curr = nc->head; curr->next; curr = curr->next){

        if(!(key && key_size && curr->key_set && (key_size == curr->key_size))){
            curr->fn_pointer(param, param_size, option);

        }else{  
            /*se a condição acima for false(não é um wildcard), então compara a chave passada por argumento com a chave especificada no element,
                e invoca a função de callback se a chave for a mesma */
            if(memcmp(key, curr->element_key, key_size) == 0){
                curr->fn_pointer(param, param_size, option);
            }
        }   

    }
}

void notification_chain_register(notification_chain_t* nc, chain_element_t* ne){
    
    /* faz uma copia do notification chain element "ne", o subscriber já tera populado
        uma struct chain_element_t */

    chain_element_t* ne_copy = calloc(1, sizeof(chain_element_t));
    memcpy(ne_copy, ne, sizeof(chain_element_t));
    add_element_into_notification_chain(nc, ne_copy);

}