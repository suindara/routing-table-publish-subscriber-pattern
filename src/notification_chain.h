#ifndef NOTIFICATION_CHAIN_H
#define NOTIFICATION_CHAIN_H

/*

    Notification chain é uma linked list com ponteiros para funções.

*/

#include<stdbool.h>
#include<stdint.h>

typedef struct notification_chain_t notification_chain_t;
typedef struct chain_element_t chain_element_t;

typedef enum{
    UNKNOWN,
	ADD,
    SUB,
	MOD,
	DEL,

} ntf_chain_option_t;

typedef void (*callback_fn)(void * /*espaço alocado*/, unsigned int/*tamanho*/, ntf_chain_option_t);

#define MAX_NOTIFICATION_CHAIN_NAME 64
#define MAX_NOTIFICATION_KEY 64

struct notification_chain_t{

    char name[MAX_NOTIFICATION_CHAIN_NAME];
    chain_element_t* head;
    chain_element_t* tail;    
};

struct chain_element_t{
    
    char element_key[MAX_NOTIFICATION_KEY];
    unsigned int key_size;
    bool key_set;
    callback_fn fn_pointer; /* função que o subscriber quer que seja executada */
    chain_element_t* next;
    unsigned int subscriber_id;
};

notification_chain_t* create_notification_chain(char* name);
/* subscription request*/
/* com essa função o subscriber se registra na notification chain */
void notification_chain_register(notification_chain_t *, chain_element_t*);
/* invoke request */                                               
void notification_chain_call(notification_chain_t *, void* /*param*/, unsigned int /*param size*/, const char* /*key*/, unsigned int /*key size*/, ntf_chain_option_t);
void add_element_into_notification_chain(notification_chain_t* , chain_element_t*);

#endif