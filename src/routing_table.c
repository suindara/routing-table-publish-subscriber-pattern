#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <assert.h>

#include "routing_table.h"

void init_routing_table(routing_table_t* rt){
    rt->head = NULL;
    rt->size = 0;
}           

/*Função chamada pelo subscriber*/
void routing_table_register_notification(routing_table_t* rt, routing_table_keys_t* rt_keys, size_t key_size, callback_fn callback_function, unsigned int subscriber_identifier){
    
    /* checa se já exister um registro para a entrada na routing table */
    routing_table_entry_t* rt_entry = peek_routing_table_entry(rt, rt_keys->destination, rt_keys->mask);

    bool new = false;

    if(rt_entry == NULL){
            /* Se não existe o registro, então criamos um novo para a entrada na tabela */
        rt_entry = add_update_routing_table(rt, rt_keys->destination, rt_keys->mask, 0, 0, INSERT);
        new = true;
    }

    /*aqui não preciso copiar a key pra notification_chain_element porque ela já está dentro da routing table*/
    chain_element_t notification_chain_element;
    assert(key_size <= MAX_NOTIFICATION_KEY);
    memset(&notification_chain_element, 0, sizeof(chain_element_t));
    notification_chain_element.fn_pointer = callback_function;
    notification_chain_element.subscriber_id = subscriber_identifier;
    /*atribui esse novo element na lista de notification_chain da routing table*/
    notification_chain_register(rt_entry->notification_chain, &notification_chain_element);

    /* se o subscriber se inscrever para uma entrada que já existe, então notifica ele com o "opcode" INSERT, usando
        a função de callback que ele passou */
    if(new == false){
        callback_function(rt_entry, sizeof(routing_table_entry_t), ADD);
    }
}

routing_table_entry_t* add_update_routing_table(routing_table_t *rt, const char* destination_addr, char mask, const char* gateway, const char* interface, entry_type type){

    routing_table_entry_t* rt_entry = peek_routing_table_entry(rt, destination_addr, mask);

    if(rt_entry == NULL){

        rt_entry = calloc(1, sizeof(routing_table_entry_t));
        strncpy(rt_entry->rt_keys.destination, destination_addr, DEST_KEY_SIZE);
        rt_entry->rt_keys.mask = mask;
        rt_entry->notification_chain = create_notification_chain(0); 
    }

    if(gateway){
        strncpy(rt_entry->gateway_ip, gateway, GATEWAY_IP_SIZE);
    }

    if(interface){
        strncpy(rt_entry->outgoing_interface, interface, OUTGOING_INTERFACE_SIZE);
    }

    if(type == INSERT){

        routing_table_entry_t* head = rt->head;
        
        if(head){
            head->next = rt_entry;
            rt_entry->prev = head;
            rt_entry->next = NULL;
        } 
        
        if(!head){
            rt->head = rt_entry;
            rt_entry->next = NULL;
            rt_entry->prev = NULL;
        }

        rt->size++;
    }

    /*Caso o publisher faça alguma alteração em qualquer entrada, avisa os subscribers */
    /* como essa função pode ser chamada tanto pelo publisher e subscriber, quando o subscriber chama, já se sabe
        a interface e o gateway, com essa verificação fica possivel saber se está sendo uma atualização do publisher
        ou criada pelo subscriber */

    if (gateway || interface){
        notification_chain_call(rt_entry->notification_chain, (char *)rt_entry, sizeof(routing_table_entry_t), 0, 0, UPDATE);
    }

    return rt_entry;
}

void dump_routing_table_entry(routing_table_t* rt){

    routing_table_entry_t* node = rt->head;

    for(node; node->next != NULL; node = node->next){
        printf("[+] %-20s %-4d %-20s %s\n", node->rt_keys.destination, node->rt_keys.mask, node->gateway_ip, node->outgoing_interface);
    }
    printf("[+] Qtde: %d", rt->size);
}

routing_table_entry_t* peek_routing_table_entry(routing_table_t* rt, const char *destination_addr, char mask){

    routing_table_entry_t* node;

    if(rt->head != NULL){

        node = rt->head;

        for(node; node->next != NULL; node = node->next){

            if(strncmp(node->rt_keys.destination, destination_addr, DEST_KEY_SIZE) == 0 && node->rt_keys.mask == mask){
                return node;
            }
        }
    }


    return NULL;
}

bool delete_routing_table_entry(routing_table_t* rt, const char* destination_addr, char mask){

    routing_table_entry_t* node = rt->head;
    
    for(node; node->next != NULL; node = node->next){
        
        if(strncmp(node->rt_keys.destination, destination_addr, DEST_KEY_SIZE) == 0 && node->rt_keys.mask == mask){
            
            /*remove o elemento da lista*/
            routing_table_entry_t* aux = node;
            node->prev->next = node->next;
            node->next->prev = node->prev;
            
            /*desaloca todos os elementos da notification chain */
            for(chain_element_t* curr = aux->notification_chain->head; curr; curr = curr->next){
                chain_element_t* entry_to_delete = curr;
                /*notifica aos subscribers sobre a remoção da entrada pelo publisher*/
                notification_chain_call(aux->notification_chain, (char *)entry_to_delete, sizeof(routing_table_entry_t), 0, 0, DEL);
                free(entry_to_delete);
            }
            /*desaloca espaço alocado para notification chain*/
            free(aux->notification_chain);
            aux->notification_chain = NULL;

            rt->size--;
            return true;
        }
    }

    return false;
}


